# Finnish translation of apt-listchanges
# Copyright (C) 2001,2002 Jaakko Kangasharju
# Jaakko Kangasharju <ashar@iki.fi>, 2001, 2002.
#
msgid ""
msgstr ""
"Project-Id-Version: apt-listchanges 2.6\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2017-07-08 22:48+0200\n"
"PO-Revision-Date: 2002-02-06 22:30+0200\n"
"Last-Translator: Jaakko Kangasharju <ashar@iki.fi>\n"
"Language-Team: Finnish <debian-l10-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../apt-listchanges.py:62
#, python-format
msgid "Unknown frontend: %s"
msgstr ""

#: ../apt-listchanges.py:79
#, python-format
msgid "Cannot reopen /dev/tty for stdin: %s"
msgstr ""

#: ../apt-listchanges.py:125
#, fuzzy, python-format
msgid "News for %s"
msgstr "Muutokset pakettiin %s"

#: ../apt-listchanges.py:127
#, python-format
msgid "Changes for %s"
msgstr "Muutokset pakettiin %s"

#: ../apt-listchanges.py:130
msgid "Informational notes"
msgstr "Tiedotukset"

#: ../apt-listchanges.py:137
#, fuzzy
msgid "apt-listchanges: News"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges.py:138
#, fuzzy
msgid "apt-listchanges: Changelogs"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges.py:144
#, fuzzy, python-format
msgid "apt-listchanges: news for %s"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges.py:145
#, fuzzy, python-format
msgid "apt-listchanges: changelogs for %s"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges.py:151
msgid "Didn't find any valid .deb archives"
msgstr "En löytänyt kelvollisia .deb-arkistoja"

#: ../apt-listchanges.py:168
#, python-format
msgid "%s: will be newly installed"
msgstr "%s on uusi asennus"

#: ../apt-listchanges.py:195
#, python-format
msgid "%(pkg)s: Version %(version)s has already been seen"
msgstr "Paketin %(pkg)s versio %(version)s on nähty aiemmin"

#: ../apt-listchanges.py:199
#, fuzzy, python-format
#| msgid "%(pkg)s: Version %(version)s has already been seen"
msgid ""
"%(pkg)s: Version %(version)s is lower than version of related packages "
"(%(maxversion)s)"
msgstr "Paketin %(pkg)s versio %(version)s on nähty aiemmin"

#: ../apt-listchanges.py:271
#, python-format
msgid "Received signal %d, exiting"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:54
msgid "Aborting"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:59
#, fuzzy, python-format
msgid "Confirmation failed: %s"
msgstr "Tiedotukset"

#: ../apt-listchanges/apt_listchanges.py:63
#, fuzzy, python-format
msgid "Mailing %(address)s: %(subject)s"
msgstr "Postitan muutoslokit osoitteeseen %s"

#: ../apt-listchanges/apt_listchanges.py:82
#, python-format
msgid "Failed to send mail to %(address)s: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:91
#, python-format
msgid "The mail frontend needs an installed 'sendmail', using %s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:97
#, python-format
msgid "The mail frontend needs an e-mail address to be configured, using %s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:110
#, fuzzy
msgid "Available apt-listchanges frontends:"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/apt_listchanges.py:112
msgid "Choose a frontend by entering its number: "
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:121
#: ../apt-listchanges/apt_listchanges.py:415
#, python-format
msgid "Error: %s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:123
#, python-format
msgid "Using default frontend: %s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:166
#, python-format
msgid "$DISPLAY is not set, falling back to %(frontend)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:186
#, python-format
msgid ""
"The gtk frontend needs a working python3-gi,\n"
"but it cannot be loaded. Falling back to %(frontend)s.\n"
"The error is: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:287
msgid "Do you want to continue? [Y/n] "
msgstr "Haluatko jatkaa? [Kyllä=y/ei=n] "

#: ../apt-listchanges/apt_listchanges.py:300
#: ../apt-listchanges/apt_listchanges.py:326
#: ../apt-listchanges/apt_listchanges.py:334
msgid "Reading changelogs"
msgstr "Luen muutoslokeja"

#: ../apt-listchanges/apt_listchanges.py:334
msgid "Done"
msgstr "Valmis"

#: ../apt-listchanges/apt_listchanges.py:363
#, fuzzy, python-format
#| msgid "%s exited with status %d"
msgid "Command %(cmd)s exited with status %(status)d"
msgstr "%s lopetti suorituksen palauttaen arvon %d"

#: ../apt-listchanges/apt_listchanges.py:397
#, python-format
msgid "Found user: %(user)s, temporary directory: %(dir)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:443
#, python-format
msgid "Error getting user from variable '%(envvar)s': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:449
msgid "Cannot find suitable user to drop root privileges"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:475
#, python-format
msgid ""
"None of the following directories is accessible by user %(user)s: %(dirs)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:505
msgid "press q to quit"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:89
#, fuzzy, python-format
msgid "Unknown configuration file option: %s"
msgstr "Tiedotukset"

#: ../apt-listchanges/ALCConfig.py:102
msgid "Usage: apt-listchanges [options] {--apt | filename.deb ...}\n"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:108
#, python-format
msgid "Unknown argument %(arg)s for option %(opt)s.  Allowed are: %(allowed)s."
msgstr ""

#: ../apt-listchanges/ALCConfig.py:120
#, python-format
msgid "%(deb)s does not have '.deb' extension"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:123
#, fuzzy, python-format
msgid "%(deb)s does not exist or is not a file"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/ALCConfig.py:126
#, fuzzy, python-format
msgid "%(deb)s is not readable"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/ALCConfig.py:217
msgid "--since=<version> and --show-all are mutually exclusive"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:225
msgid "--since=<version> expects a path to exactly one .deb archive"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:118 ../apt-listchanges/DebianFiles.py:127
#, python-format
msgid "Error processing '%(what)s': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:273
#, python-format
msgid "Calling %(cmd)s to retrieve changelog"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:277
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; 'apt-get changelog' failed "
"with: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:283
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; could not run 'apt-get "
"changelog': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:322
#, python-format
msgid "Ignoring `%s' (seems to be a directory!)"
msgstr ""

#: ../apt-listchanges/AptListChangesGtk.py:42
#, fuzzy
msgid "apt-listchanges: Reading changelogs"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/AptListChangesGtk.py:43
#, fuzzy
msgid "Reading changelogs. Please wait."
msgstr "Luen muutoslokeja"

#: ../apt-listchanges/AptListChangesGtk.py:75
msgid "Continue Installation?"
msgstr ""

#: ../apt-listchanges/AptListChangesGtk.py:75
msgid "You can abort the installation if you select 'no'."
msgstr ""

#: ../apt-listchanges/ALCApt.py:59
msgid "APT pipeline messages:"
msgstr ""

#: ../apt-listchanges/ALCApt.py:66
msgid "Packages list:"
msgstr ""

#: ../apt-listchanges/ALCApt.py:74
#, fuzzy
#| msgid ""
#| "Wrong or missing VERSION from apt pipeline\n"
#| "(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgid ""
"APT_HOOK_INFO_FD environment variable is not defined\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD set to 20?)"
msgstr ""
"Väärä tai puuttuva VERSION putkessa aptilta\n"
"(onko Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version arvoltaan 2?)"

#: ../apt-listchanges/ALCApt.py:80
msgid "Invalid (non-numeric) value of APT_HOOK_INFO_FD environment variable"
msgstr ""

#: ../apt-listchanges/ALCApt.py:83
#, python-format
msgid "Will read apt pipeline messages from file descriptor %d"
msgstr ""

#: ../apt-listchanges/ALCApt.py:86
msgid ""
"Incorrect value (0) of APT_HOOK_INFO_FD environment variable.\n"
"If the warning persists after restart of the package manager (e.g. "
"aptitude),\n"
"please check if the /etc/apt/apt.conf.d/20listchanges file was properly "
"updated."
msgstr ""

#: ../apt-listchanges/ALCApt.py:91
#, fuzzy
#| msgid ""
#| "Wrong or missing VERSION from apt pipeline\n"
#| "(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgid ""
"APT_HOOK_INFO_FD environment variable is incorrectly defined\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD should be greater "
"than 2)."
msgstr ""
"Väärä tai puuttuva VERSION putkessa aptilta\n"
"(onko Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version arvoltaan 2?)"

#: ../apt-listchanges/ALCApt.py:97
#, python-format
msgid "Cannot read from file descriptor %(fd)d: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/ALCApt.py:103
msgid ""
"Wrong or missing VERSION from apt pipeline\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgstr ""
"Väärä tai puuttuva VERSION putkessa aptilta\n"
"(onko Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version arvoltaan 2?)"

#: ../apt-listchanges/ALCLog.py:30 ../apt-listchanges/ALCLog.py:36
#, fuzzy, python-format
msgid "apt-listchanges: %(msg)s"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/ALCLog.py:33
#, fuzzy, python-format
msgid "apt-listchanges warning: %(msg)s"
msgstr "apt-listchangesin tulostus koneelta %s"

#: ../apt-listchanges/ALCSeenDb.py:56
msgid ""
"Path to the seen database is unknown.\n"
"Please either specify it with --save-seen option\n"
"or pass --profile=apt to have it read from the configuration file."
msgstr ""

#: ../apt-listchanges/ALCSeenDb.py:67
#, python-format
msgid "Database %(db)s does not end with %(ext)s"
msgstr ""

#: ../apt-listchanges/ALCSeenDb.py:76
#, python-format
msgid "Database %(db)s failed to load: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/apt-listchanges.ui:13
msgid "List the changes"
msgstr ""

#: ../apt-listchanges/apt-listchanges.ui:43
msgid ""
"The following changes are found in the packages you are about to install:"
msgstr ""

#, fuzzy
#~ msgid "The %s frontend is deprecated, using pager"
#~ msgstr "Newt-liittymä on vanhentunut, käytän sivutinta"

#~ msgid "%s exited with signal %s"
#~ msgstr "%s lopetti suorituksen signaaliin %s"
